from novamud import Dungeon, Room, Thing


class MetroTicket(Thing):
    name = 'MetroTicket'
    description = 'You need to purchase a metro ticket to make it to Cais Metro Station.'


class TrainTicket(Thing):
    name = 'TrainTicket'
    description = 'You need to purchase a train ticket to make it to Oeiras Train Station.'


class Alameda(Room):
    name = 'Alameda'
    description = ('You need to make it to Nova in time for class!'
                   ' But before, remember to purchase a metro ticket to make it to Cais Metro Station.') 
                 
    def init_room(self):
        self.add_thing(MetroTicket())

    def go_to(self, player, other_room_name):
        if not player.carrying or player.carrying.name != 'MetroTicket':
            player.tell('You need to purchase a metro ticket to make it to Cais Metro Station!')
        else:
            super().go_to(player, other_room_name)
    
    
class CaisMetroStation(Room):
    name = 'CaisMetroStation'
    description = ('Now that you are at Cais Metro Station, you still need to catch the train to Oeiras Train Station.'
                   ' Just go to Cais Train Station and purchase a ticket.')


class CaisTrainStation(Room):
    name = 'CaisTrainStation'
    description = ('Now that you at Cais Train Station, dont forget to purchase a train ticket to make it to Oeiras Train Station.')
    
    def init_room(self):
        self.add_thing(TrainTicket())

    def go_to(self, player, other_room_name):
        if not player.carrying or player.carrying.name != 'TrainTicket':
            player.tell('You need to purchase a train ticket to make it to Oeiras Train Station!')
        else:
            super().go_to(player, other_room_name)


class Train (Room):
   name = "Train"
   description = ('You are now inside the train. Listen to some music, read a book, or update Instagram.'
                  ' Dont miss the exit in Oeiras!')
   
   def go_to(self, player, other_room_name):
        super().go_to(player, other_room_name)


class OeirasTrainStation(Room):
    name = 'OeirasTrainStation'
    description = ('You are now at Oeiras Train Station. To reach Nova you must leave the train station and walk for 15 minutes.'
                   ' But before, remember to swipe your train ticket to leave the train station.')
       
    def add_player(self,player):
        player.ticket_swipped = False
        super().add_player(player)
        
    def register_commands(self):
        return ['swipe_ticket']
    
    def swipe_ticket(self, player):
        player.ticket_swipped = True
        player.tell('After swiping your ticket you can now go outside {}'.format(player.name))

    def swipe_ticket_describe(self):
        return 'You need to swipe your train ticket to leave the train station.'
    
    def go_to(self, player, other_room_name):
        if player.ticket_swipped:
            super().go_to(player, other_room_name)
        else: 
            player.tell('You need to swipe your train ticket to leave the train station.')  

class Outside(Room):
    name = 'Outside'
    description = ('Now that you are outside the train station, you need to walk to Nova for 15 minutes.')
        
    def add_player(self,player):
        player.walked = False
        super().add_player(player)    
   
    def register_commands(self):
        return ['walk']
    
    def walk(self,player):
        player.walked = True
        player.tell('Enjoy the fresh air while you walk to Nova for 15 minutes')
        
    def walk_describe(self):
        return 'I hope you brought your fitbit because you still need to walk to reach nova!'
    
    def go_to(self, player, other_room_name):
        if player.walked:
            super().go_to(player, other_room_name)
        else:
            player.tell ('You better walk!')
    
    
class Nova(Room):
    name = "Nova"
    description = ('Now that you have reached Nova, you must go to class.'
                   'Remember to message Sam on Slack so he can open the door for you!')
    
    def add_player(self,player):
        player.messaged_sam = False
        super().add_player(player)   
    
    def register_commands(self):
        return ['message_sam']
    
    def message_sam(self, player):
        player.messaged_sam = True
        player.tell ('You have just asked Sam on Slack to open the door for you and let you inside the classroom')
    
    def message_sam_describe(self):
        return 'Now you need to message Sam on Slack and ask him to open the door for you and let you inside the classroom'
    
    def go_to(self, player, other_room_name):
        if player.messaged_sam:
            super().go_to(player, other_room_name)
        else:
            player.tell('You have to message Sam first. Only he can let you in!')


class Classroom(Room):
    name = 'Classroom'
    description = ('Have fun learning learn a bunch of stuff!!')
    
    
class RoadToNovaDungeon(Dungeon):
    name = 'RoadToNova'
    description = ('With the move of the Campus to Carcavelos you need to take quite a long journey to reach the University.'
                   ' Have fun going from Alameda to the beach!!')

    def init_dungeon(self):
        alameda = Alameda(self)
        caismetrostation = CaisMetroStation(self)
        caistrainstation = CaisTrainStation(self)
        train = Train(self)
        oeirastrainstation = OeirasTrainStation(self)
        outside = Outside(self)
        nova = Nova(self)
        classroom = Classroom(self)
        
        alameda.connect_room(caismetrostation, two_way=True)
        caismetrostation.connect_room(caistrainstation, two_way=True)
        caistrainstation.connect_room(train, two_way=True)
        train.connect_room(oeirastrainstation, two_way=True)
        oeirastrainstation.connect_room(outside, two_way=True)
        outside.connect_room(nova, two_way=True)
        nova.connect_room(classroom, two_way=True)
 
        self.start_room = alameda


if __name__ == '__main__':
    RoadToNovaDungeon().start_dungeon()