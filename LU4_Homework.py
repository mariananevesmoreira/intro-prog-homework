#EXERCISE 1 - Power

def power(a,b):
    return a**b

a=10 #just to test
b=2 #just to test
print(power(a,b)) #just to test


#EXERCISE 2 - Equal

def test_equal(variable_1,variable_2):
    return variable_1==variable_2

variable_1=5 #just to test
variable_2=5 #just to test
print(test_equal(variable_1,variable_2)) #just to test


#EXERCISE 3 - Hypotenuse

def hypotenuse(side_1,side_2):
    return_value=(side_1**2)+(side_2**2)
    return return_value**0.5

side_1=3 #just to test
side_2=4 #just to test
print(hypotenuse(side_1,side_2)) #just to test


#EXERCISE 4 - Cryptocurrency

def bitcoin_litecoin(bitcoin):
    bitcoin_litecoin_rate=124.66
    return bitcoin*bitcoin_litecoin_rate

def bitcoin_ethereum(bitcoin):
    bitcoin_ethereum_rate=31.71
    return bitcoin*bitcoin_ethereum_rate

def bitcoin_euro(bitcoin):
    bitcoin_euro_rate=5612.18
    return bitcoin*bitcoin_euro_rate

bitcoin=100 #just to test
print(bitcoin_litecoin(bitcoin)) #just to test
print(bitcoin_ethereum(bitcoin)) #just to test
print(bitcoin_euro(bitcoin)) #just to test


#EXERCISE 5 

def no_return():
    return

print(no_return())
print(type(no_return()))

#Returns 'none' and type is 'NoneType'