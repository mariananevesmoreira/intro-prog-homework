#EXERCISE 1

class Robot:
   
    def __init__(self):
        self.recordings = []
    
    def listen(self, string):
        self.recordings.append(string)
    
    def play_recordings(self):
        print(self.recordings)
    

robot_1 = Robot()

robot_2 = Robot()

robot_1.listen('hello')

robot_2.listen('whats up?')

robot_1.play_recordings() #'hello'

robot_2.play_recordings() #'whats up?'


#They listen and play different things because they are different instances of robots. Each time one is created, the constructor creates a new recordings array for each. They do not share the same recordings array because they are different instances


#EXERCISE 2

class Robot:
   
    def __init__(self):
        self.recordings = []
    
    def listen(self, string):
        self.recordings.append(string)
    
    def play_recordings(self):
        print(self.recordings)
    
    def delete_recordings(self):
        self.recordings.clear()


robot_3 = Robot()

robot_3.listen('hello world')

robot_3.play_recordings() #'hello world'

robot_3.delete_recordings()

robot_3.play_recordings() #[]

robot_3.listen('hello again')

robot_3.play_recordings() #'hello again'


#EXERCISE 3

robot_4 = Robot()

robot_5 = Robot()

robot_4.listen('goodbye world')

robot_5.listen('winter is coming')

robot_4.play_recordings()

robot_5.play_recordings()

robot_4.delete_recordings() #just the recording of robot_4 gets deleted

robot_4.play_recordings()

robot_5.play_recordings()


#EXERCISE 4

class LectureRoom:
    
    def __init__(self):
        self.capacity = 40
       
    def increase_capacity(self, amount):
        if self.capacity + amount < 100 and self.capacity + amount > 10:
            self.capacity = self.capacity + amount
        else:
            print ('invalid capacity, must be between 40 and 100')
    
    def decrease_capacity(self, amount):
        if self.capacity - amount < 100 and self.capacity - amount > 10:
            self.capacity = self.capacity - amount
        else:
            print ('invalid capacity, must be between 40 and 100')
        
class_1 = LectureRoom()

class_1.increase_capacity(80)

print(class_1.capacity)

class_1.decrease_capacity(10)

print(class_1.capacity)



