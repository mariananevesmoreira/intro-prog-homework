#EXERCISE 1

#Scope: scope has to do with the visibility of variables and functions; it has to do with where a variable or function can be used. There are two types of scope (that we have learned): global scope (all functions inside of a file) and local scope (what's inside of the function). The local scope can access the global scope, whereas the global scope cannot access the local scope.


#EXERCISE 2

def hello_wordl():
    who = 'world'
    print('hello', who) #what is inside the function is in the local scope
    
second_var = 20 #global scope

def hello_other_world():
    who_again = 'alien_world'
    print(who_again) #what is inside the function is in the local scope


#EXERCISE 3

print('hello', first_var) #no error, this prints hello 10

first_var = 10


#EXERCISE 4

variable_1 = 5

def no_arguments():
    return variable_1 * 2

variable_2 = no_arguments() + 10

print(no_arguments())

print(variable_2)


#EXERCISE 5

power = 10

print(power) #will print 10

def generate_power(number):
    _power_ = 2
    def nth_power():
        return number ** power
    powered = nth_power()
    return powered

a = generate_power(2)

print(a) #error with print(_power_), _power_ is in the local scope and the global scope cannot access it

#if we remove print(_power_) print(a) will print 2 to the power of 10 (1024) - the function will access the global scope for the value of power and a is defined in the global scope using the output of the function


#EXERCISE 6

power_2 = 100
print(power_2)

def generate_power_2(number_2):
    power_2 = 2
    
    def nth_power_2():
        return number_2 ** power_2
    
    powered_2 = nth_power_2()
    return powered_2

print(power_2)

a = generate_power_2(2)
print(a)

#print(power_2) will be the same because if the print is in the global scope it will only take the value assigned to power_2 that is in the global scope (it cannot access the local scope) - 100

#print(a) will be 2 to the power of 2 because this is in the local scope, and will therefore take the value assigned to power_2 that is in the local scope - 2

#the variable power_2 is constantly changing scopes (it is in the global scope for print(power_2) but in the local scope of the function generate_power_2 as well)


#EXERCISE 7

_what = 'coins'
_for = 'chocolates'

def transaction (_what, _for):
    _what = 'oranges'
    _for = 'bananas'
    print(_what, _for) #prints oranges bananas
    
transaction (_what, _for)

print(_what, _for) #prints coins chocolates

#it will print the information that is available in each scope


#EXERCISE 9

def name (first_name = 'Mariana', last_name = 'Moreira'):
    print (first_name, last_name)

name() #prints Mariana Moreira

name('Mariana', 'Moreira') #prints Mariana Moreira (if it was name(first_name, last_name) it would give an error as these are not defined in the global scope)

name('Sofia', 'Moreira') #prints Sofia Moreira


#EXERCISE 11

def greetings (message = 'Good Morning', name = ''):
    print(message, name)

greetings() #returns Good Morning

greetings(message = 'You know nothing', name = 'Jon Snow') #returns You know nothing Jon Snow