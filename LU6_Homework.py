#EXERCISE 0

my_tuple = ('a', 'b', 'c')

#the index of 'a' is 0; the index of 'b' is 1; the index of 'c' is 2

my_tuple[0] #will return 'a' (starts at the 0th element)

my_tuple[1] #will return 'b'

my_tuple[2] #will return 'c'

my_tuple[0:3] #will return all elements in the tuple (3rd element is not included)

my_tuple [0:2] #will return 'a' and 'b'

print(type(my_tuple[0:2])) #will print class 'tuple' - a slice of a tuple is still a tuple


#EXERCISE 1

#tuple[0] = 5 #error - tuples are for when we don't need the elements to change


#EXERCISE 2

tuple_APLL = (77,109,105,118,170)


#EXERCISE 3

#tuple_APLL.sort is not possible. We cannot sort a tuple; tuples are for holding information that does not change


#EXERCISE 4

list_APLL = [77,109,105,118,170]

list_APLL.append (200) #this will add 200 to the end (append - add to the end)

list_APLL[0] = 300 #this will change the 0th element to 300

list_APLL.sort() #this will sort from lowest to highest

list_APLL.sort(reverse=True) #this will sort from highest to lowest

#lists are for changing


#EXERCISE 5

tuple_1 = (0,1,2)

tuple_2 = (0,1,2)

print(tuple_1==tuple_2) #this will print True as both tuples have the same elements in the same index

tuple_3 = (1,2,0)

print(tuple_1==tuple_3) #this will print False because although the tuples have the same elements, they are not in the same index

list_1 = [0,1,2]

print(tuple_1==list_1) #this will print False. A tuple is never equal to a list even if they have the same element in the same indez


#EXERCISE 8

stock_price = {
        'APLLa':77, 
        'APLLb':105, 
        'APLLc':118, 
        'APLLd':170
        }

#dictionary holds the key-value pair of the stock of APLL for each year

stock_price ['APLLb'] #will return 105

print(stock_price.keys()) #will print dict_keys(['APLLc', 'APLLd','APLLb', 'APLLa'])

print(stock_price.values()) #will print dict_values([118,170,105,77])

print(stock_price.items()) #will print dict_items([('APLLc,118), ('APLLd', 170), ('APLLb', 105), ('APLLa', 77)])


#EXERCISE 9

new_dictionary = {
        'hello':'world'
        }

new_dictionary['goodnight'] = 'other_world'

print(new_dictionary) #will print the original new_dictionary with the element that was added {'goodnight':'other_world', 'hello':'world}


#EXERCISE 10

new_stock_prices = {
        'APLL':100,
        'GOOG':99
        }

stock_of_interest = 'GOOG'

print(new_stock_prices[stock_of_interest]) #will print the value associated with the key 'GOOG' - 99


#EXERCISE 12

#delete key value pair from a dictionary


#EXERCISE 13

def append(a_list):
    a_list.append('homie')
    return a_list

a_list=[10,20,30] #just to test

print(append(a_list)) #just to test


#EXERCISE 14

def stocks(a_dictionary):
    sum_dictionary = sum(a_dictionary.values())
    divide_dictionary = sum_dictionary/7
    return divide_dictionary

a_dictionary = {
        'GOOG':100,
        'APLL':50,
        'KPMG':10,
        'BCG':5,
        'MSFT':500,
        'OSYS':200,
        'PHZR':20
        }

print(stocks(a_dictionary)) #just to test