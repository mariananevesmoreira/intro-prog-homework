#EXERCISE 1

print(bool(1)) #True - bool expression tests the boolean value; print is just to see what it prints

print(bool(1.1)) #True

print(bool(0)) #False

print(bool(0.0001)) #True

print(bool('')) #False

print(bool('None')) #True


#EXERCISE 2

def ticker_symbol(stock):
    if stock == 'APLL':
        return True
    else:
        return False

print(ticker_symbol('GOOG')) #false

#EXERCISE 3

def pink(gender):
    if gender == 'Female':
        return 'You hate pink'
    elif gender == 'Male':
        return 'You like pink'

print(pink('Female')) #you hate pink


#EXERCISE 4

#the last print will make the function print 'This line shouldnt be printed' anyway (this is not included in any of the if statements)

def gender(new_gender):
    if new_gender == 'male':
        print('You hate pink')
    if new_gender == 'female':
        print('You like pink')
    print('This line shouldnt be printed')
        
gender('male') #you hate pink + this line shouldnt be printed


#EXERCISE 5

def age(age_mother, age_father):
    if age_mother == age_father:
        return age_mother + age_father
    elif age_mother != age_father:
        return age_mother - age_father

print(age(53,50)) #3


#EXERCISE 6

def dict_list(one_dictionary, one_list):
    if len(one_list) == len(one_dictionary): #lenghts are equal
        return 'awesome'
    elif len(one_list) != len(one_dictionary):
        return 'this is sad'

one_dictionary = {
        'ABC':1,
        'DEF':2,
        'GHI':3
        }

one_list = [4,5,6,7]

print(dict_list(one_dictionary,one_list)) #'this is sad'


#EXERCISE 7

another_dictionary = {
        'APLL':201,
        'GOOG':1040,
        'FB':148}

for key in another_dictionary:
    print(key)

for key in another_dictionary:
    print(another_dictionary[key])

for key in another_dictionary:
    print('the stock price of',key,'is',another_dictionary[key])


#EXERCISE 8

my_tuple = (10,20,30)

my_list = [40,50,60]

my_dictionary = {
        'APPL':100,
        'GOOG':90,
        'FB':80
        }

for num in my_tuple:
    print(num)

for num in my_list:
    print(num)

for key in my_dictionary:
    print(key,my_dictionary[key])


#EXERCISE 9
    
def single_list(one_more_list = []):
    new_list = []
    for i in one_more_list:
        new_list.append(i * 2)
    return new_list

print(single_list(one_more_list = [10,20,30])) #[20,40,60]


#EXERCISE 10

def single_dictionary(one_more_dictionary = {}): 
    new_dictionary = {}
    for key in one_more_dictionary:
        new_dictionary[key] = 2 * one_more_dictionary[key]
    return new_dictionary

print(single_dictionary(one_more_dictionary = {
        'APPL':100,
        'GOOG':90,
        'FB':80
        })) #{'APPL':200, 'GOOG':180, 'FB':160}


#EXERCISE 11

def function(last_list,num):
    if len(last_list) > num:
        return True

last_list = [1,2,3,4,5]

num = 4

print(function(last_list,num)) #True