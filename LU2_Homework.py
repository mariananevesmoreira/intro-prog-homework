#EXERCISE 1 - REPL to text editor

ticker_symbol='TSLA' #string variable
value_shares=330.90 #float variable
trade_shares=100 #integer variable
action='buy' #string variable
print('I would like to', action,trade_shares, 'shares in', ticker_symbol) #print does not return anything, it just presents it for humans to see


#EXERCISE 2 - Print useful messages

person_1='Mariana'
person_2='Sofia'
borrow=1000
shares=10
company='APPL'
print(person_1,'needs to borrow', borrow, 'euros from', person_2, 'in order to trade', shares, 'stocks in', company)


#EXERCISE 3 - Multiply two variables

variable_1=10
variable_2=20
multiply=variable_1*variable_2
result=multiply
print(result)


#EXERCISE 4 - Conversion to fahrenheit and kelvin

celsius=30
fahrenheit=celsius*1.8+32
kelvin=celsius+273.15
print(fahrenheit)
print(kelvin)


#EXERCISE 5 - Return value

1
'hello'
'what'+'is'+'my'+'name'
30/5

print(1) #not a return
print('hello') #not a return
print('what '+'is '+'my '+'name ') #not a return
print(30/5) #not a return


#EXERCISE 6 - Variable Assignment Return Value

variable_3=100 #we knoe the expression that assigns a value to a variable has a return value if there is a red 0ut in the REPL
print(variable_3) #not a return